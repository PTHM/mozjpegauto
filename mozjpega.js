#! /usr/bin/env node

var dir = require('node-dir');
var sys = require('sys');
var path = require('path');
var mkdirp = require('mkdirp');
var execFile = require('child_process').execFile;
var mozjpeg = require('mozjpeg').path;


dir.files(process.cwd(), function(err, files) {
  if (err) throw err;

  mkdirp(process.cwd()+'/compressed', function(err) {
    if (err) throw err;
  });

  //we have an array of files now, so now we'll iterate that array
  files.forEach(function(p) {
    if(path.extname(p) == '.jpg' || path.extname(p) == '.jpeg'){
      execFile(mozjpeg, ['-outfile', 'compressed/'+path.basename(p), p], function (err) {
          if (err) { throw err; }
          console.log("Processed: "+ path.basename(p));
      });

    }
  });
});